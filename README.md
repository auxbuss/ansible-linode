# ansible-create-linode

An ansible playbook to provision a new linode server.

Warning: Every run creates a new Linode server.

## Defaults:

The default configuration (`vars/config.yml`) creates:

  * 1 GB Linode 1024 (linode_plan: 1)
  * 256 MB swap (linode_swap: 256)
  * Ubuntu 16.04 LTS (linode_distribution: 146)
  * Newark, NJ, USA (linode_datacenter: 6)

### Architecture

    # dpkg --print-architecture
    > amd64
    # dpkg --print-foreign-architectures
    > i386
    # getconf LONG_BIT
    > 64

## Usage:

Change variables in `vars/config.yml` to suit your needs.

```
$ export LINODE_API_KEY=<your Linode API key>
$ ansible-playbook --extra-vars server_hostname=<your hostname> site.yml
$ ssh root@<server IP address>
```

## Rebuild the server, yet keep the IP

This can be useful, for example, when you want to recreate your server from scratch and want to keep your existing DNS settings.

  * Login to your server as root and add a password via `passwd`
  * Go to the server's `Rebuild` tab in Linode Manager
  * Select an image (Ubuntu 16.04 LTS), enter your server's root password, click `Rebuild`, click `OK`
  * Wait in the Dashboard for the server to rebuild, then click `Boot`

At this point, attempting to login (including ansible) via ssh will fail due to a now invalid key in your local `.ssh/known/hosts`.

  * Run `ssh-copy-id root@<server IP>`, which will fail and display the offending line number in `.ssh/known/hosts`.
  * Run `ssh-keygen -R <offending line number> <server IP>` to remove the offending line number in `.ssh/known/hosts`.
  * Run `ssh-copy-id root@<server IP>` again, which should now succeed. (Requires your server's root password.)
  * You can now login to your server as root: `ssh root@<server IP>`

You can now configure your server with ansible:

  * First, add your `<server IP>` to the `[linode]` group in `/inventory` file (otherwise ansible will fail with `no hosts matched`)

```
$ ansible-playbook --extra-vars server_hostname=<your hostname> configure.yml
```
